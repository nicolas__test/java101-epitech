package net.epitech.java.td01.td0ioc.model.contract;

import net.epitech.java.td01.td0ioc.model.types.SexType;

/**
 * 
 * @author nicolas
 * It represents an entity that can be addressed by a name
 *
 */
public interface Nameable {
	/**
	 * 
	 * @return public name of a nameable person
	 * 
	 */
	public abstract String getName();
	/**
	 * 
	 * @return your sex
	 */
	public abstract SexType getSex();

	// TODO change interface
}