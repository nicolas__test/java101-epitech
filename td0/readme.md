# TD0 : Injection de dépendance, MVC et Applications web

## Objectifs du TD

### Rappels
+ Récap du TD00
+ Point sur le partage du matériel pédagogique (bitbuckets, fork, framapad)

### Techniques
+ Fondement du JavaWeb, ma première servlet
+ Le nouveau new: Présentation de l'IOC et injection de dépendance
+ Spring et l''IOC
+ Présentation du pattern Model-View-Controler
+ Implémentation du MVC Avec Spring

## TD00

+ Framapad pour la prise de note collaborative en direct: http://lite3.framapad.org/p/java101
+ Responsabilité des **étudiants** (aucune autre notre ne sera transmise)
+ Un repository Bitbucket est mis à votre disposition avec la documentation courrante: il contient le module parent, ainsi qu''un module enfant pour chaque td
+ un squelette de code est fourni en support de chaque td (en cas de besoin), le code du td est mis à jour par Nicolas ou par les étudiants pendant le TD.
+ Les étudiants peuvent forker le repository, expérimenter et soumettre leur modifications par pull request.

## Ma première servlet (td0-servlet)


> mvn archetype:generate -DgroupId=com. \
>                       -DartifactId=my-webapp \
>                       -Dversion=1.0-SNAPSHOT \
>                       -DarchetypeArtifactId=maven-archetype-webapp \
>                       -DinteractiveMode=false



## IOC

> object coupling is bound at run time by an assembler object and is typically not known at compile time using static analysis.
[http://en.wikipedia.org/wiki/Inversion_of_control]

Intérets de l'IOC:

>   There is a decoupling of the execution of a certain task from implementation.
>   Every module can focus on what it is designed for.
>   Modules make no assumptions about what other systems do but rely on their contracts.
>   Replacing modules has no side effect on other modules.
>   Unit Testing

## Concrétisation du pattern.

### Injection de dépendance

3 rôles pour l'injection de dépendance [http://en.wikipedia.org/wiki/Dependency_injection]

>   a dependent consumer,
>   a declaration of a component's dependencies, defined as interface contracts,
>   an injector (sometimes referred to as a provider or container) that creates instances of classes that implement a given dependency interface on request.

### le Framework Spring: micro container

mvn archetype:generate     -DarchetypeGroupId=com.github.spring-mvc-archetypes     -DarchetypeArtifactId=spring-mvc-quickstart  
